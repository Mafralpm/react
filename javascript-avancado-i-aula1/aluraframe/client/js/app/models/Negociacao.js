class Negociacao {

    constructor(data, quanditidade, valor) {

        this._data = new Date(data.getTime());
        this._quanditidade = quanditidade;
        this._valor = valor;

        Object.freeze(this);
    };

    get data(){
        return new Date(this._data.getTime());
    }

    get quantidade(){
        return this._quanditidade;
    }

    get valor(){
        return this._valor;
    }

    get volume(){
        return this._quanditidade * this._valor;
    }
}